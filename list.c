#include <stdlib.h>
#include "common.h"
#include "list.h"

void list_init(struct list **l)
{
	if ((*l = malloc(sizeof(struct list))) == NULL) {
		puts("Not enough memory.");
		exit(EXIT_FAILURE);
	}
	(*l)->head = NULL;
}

void node_push(struct list *l, char *item)
{
	struct node *current;
	
	if ((current = malloc(sizeof(struct node))) == NULL) {
		puts("Not enough memory.");
		exit(EXIT_FAILURE);
	}

	current->data = item;

	current->prev = l->head;
	l->head = current;
}
void node_pull(char **item, struct list *l)
{
	struct node *tmp;

	if(l->head != NULL) {
		*item = l->head->data; 

		tmp = l->head->prev;
		free(l->head);
		l->head = tmp;
	}
}

void list_prnt(struct list *l)
{
	struct node *current = l->head;

	while(current != NULL) {
		printf("%s\n", current->data);
		current = current->prev;
	}
}
/*
long long list_cnt(struct list *l)
{
	struct node *current = l->head;
	long long n = 0;

	while(current != NULL) {
		++n;
		current = current->prev;
	}
	return n;
}
*/
int list_empty(struct list *l)
{
	return l->head != NULL ? 0 : 1;
}

void list_free(struct list **l)
{
	struct node *tmp;

	while((*l)->head != NULL) {
		tmp = (*l)->head->prev;
		free((*l)->head);
		(*l)->head = tmp;
	}
	free(*l);
	*l = NULL;
}
