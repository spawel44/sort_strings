#include <stdlib.h>
#include "common.h"

#define BUF_LEN 3

char *str_load(FILE *f)
{
	/* ignore empty characters at the beginning of a line */
	int c;
	while (((c = fgetc(f)) == '\n' || c == ' ' || c == '\t') && c != EOF);

	if (c == '\n' || c == EOF)
		return NULL;
	
	ungetc(c, f);

	size_t len = BUF_LEN;
	char *buf;
	if ((buf = malloc(len)) == NULL) {
		puts("(1) Not enough memory.");
		exit(EXIT_FAILURE);
	}
	
	fgets(buf, len, f);
	len = strlen(buf);
	while (buf[len - 1] != '\n') {
		size_t tmp = len;
		len += len;
		if ((buf = realloc(buf, len)) == NULL) {
			puts("(2) Not enough memory.");
			exit(EXIT_FAILURE);			
		}
		fgets(buf + tmp, len - tmp, f);
		len = strlen(buf);
	}
	
	/* shrink the buf size to match the string length */
	if ((buf = realloc(buf, len + 1)) == NULL) {
		puts("(3) Not enough memory.");
		exit(EXIT_FAILURE);
	}
	
	return buf;
}
