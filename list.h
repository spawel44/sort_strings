#ifndef LIST_H
#define LIST_H

struct node {
	char *data;

	struct node *prev;
};

struct list {
	struct node *head;
};

void list_init(struct list **l);
void node_push(struct list *l, char *item);
void node_pull(char **item, struct list *l);
/* void node_cpy(char **ap, struct list *l); */
void list_prnt(struct list *l);
/* long long list_cnt(struct list *l); */
int list_empty(struct list *l);
void list_free(struct list **l);

#endif /* LIST_H */
