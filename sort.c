#include <ctype.h>
#include "common.h"

void sort(char *ap[], const long long len, const int srt_fn_num);
static void quicksort(char *ap[], long long lo, long long hi,
			int (*cmp) (const char *, const char *), const int dir_sign);
static inline int cmp_ascii(const char *s1, const char *s2);
static inline int cmp_wrd_len(const char *s1, const char *s2);
static inline int cmp_str_len(const char *s1, const char *s2);

void sort(char *ap[], const long long len, const int srt_fn_num)
{
	int dir_sign = 1;
	int (*cmp) (const char *, const char *) = NULL;
	
	switch(srt_fn_num) {
		case 0:
			cmp = &cmp_ascii;
			break;
		case 1:
			dir_sign = -1;
			cmp = &cmp_ascii;
			break;
		case 2:
			cmp = &cmp_wrd_len;
			break;
		case 3:
			dir_sign = -1;
			cmp = &cmp_wrd_len;
			break;
		case 4:
			cmp = &cmp_str_len;
			break;
		case 5:
			dir_sign = -1;
			cmp = &cmp_str_len;
			break;
	}
	quicksort(ap, 0, len - 1, cmp, dir_sign);
}

/* UNSTABLE QUICKSORT IMPLEMENTATION */
void quicksort(char *ap[], long long lo, long long hi,
		int (*cmp) (const char *, const char *), const int dir_sign)
{
	long long i, j;
	char *xstr;
	xstr = ap[(lo + hi)>>1];
	i = lo;
	j = hi;
	do {
		while ((cmp(ap[i], xstr) * dir_sign) < 0) ++i;
		while ((cmp(ap[j], xstr) * dir_sign) > 0) --j;
		if (i <= j) {
			char *tmp = ap[i];
			ap[i] = ap[j];
			ap[j] = tmp;
			++i; --j;
		}
   	} while(i < j);
	if (lo < j) quicksort(ap, lo, j, cmp, dir_sign);
	if (hi > i) quicksort(ap, i, hi, cmp, dir_sign);
} 

static inline int cmp_ascii(const char *s1, const char *s2)
{
	long long i = 0;

	while ((s1[i] - '0') == (s2[i] - '0') && s1[i] != '\0' && s2[i] != '\0')
		++i;

	if ((s1[i] - '0') < (s2[i] - '0'))
		return -1;
	else if ((s1[i] - '0') > (s2[i] - '0'))
		return 1;
	else
		return 0;
}

static inline int cmp_wrd_len(const char *s1, const char *s2)
{
	long long i = 0, j = 0;

	while(s1[i] != '\n' && s1[i] != ' ' && s1[i] != '\t')
		++i;
	while(s2[j] != '\n' && s2[j] != ' ' && s2[j] != '\t')
		++j;

	if (i < j)
		return -1;
	else if (i > j)
		return 1;
	else
		return 0;
}

static inline int cmp_str_len(const char *s1, const char *s2)
{
	long long i, j;

	i = strlen(s1);
	j = strlen(s2);
	
	if (i < j)
		return -1;
	else if (i > j)
		return 1;
	else
		return 0;
}
