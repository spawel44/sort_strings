#CFLAGS = -Wall -Wextra -o2 -g
CFLAGS = -Wall -Wextra -Wdouble-promotion -O3
CC = gcc
sort_strings: sort_strings.c
	$(CC) sort_strings.c str_load.c list.c sort.c $(CFLAGS) -o sort_strings

clean:
	-rm -f sort_strings
