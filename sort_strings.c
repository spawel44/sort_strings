#include <stdlib.h>
#include "common.h"
#include "list.h"

#define FSIZE 255

int argck(char **argv, int i, char **ap, int len);
void print_help(void);
void str_save(FILE *f, char *ap[], const long long len);

extern char *str_load(FILE *f);
extern void sort(char *ap[], const long long len, const int srt_fn_num);

int main(int argc, char *argv[])
{
	if (argc < 5) {
		print_help();
		exit(EXIT_FAILURE);
	}

	char *apargs[] = { "-h", "--sort" };
	char *apsort[] = { "aa", "ad", "wa", "wd", "sa", "sd" };

	FILE *f;
	int srt_fn_num = 0;
	char save_fname[FSIZE + 1];

	for (int i = 1; i < argc; ++i) {
		switch(i) {
			case 1:
				if (argck(argv, i, apargs, sizeof(apargs) /
						sizeof(apargs[0])) == 0) {
					print_help();
					exit(EXIT_SUCCESS);
				}
				if ((f = fopen(argv[i], "r")) == NULL) {
					printf("%s - no such file.\n", argv[i]);
					exit(EXIT_FAILURE);
				}
				break;
			case 2:
				/* don't allow file name to be the same as arguments */
				if (argck(argv, i, apargs, sizeof(apargs) /
						sizeof(apargs[0])) != -1) {
					print_help();
					exit(EXIT_FAILURE);
				}
				if (strlen(argv[i]) > FSIZE) {
					puts("File name too long.");
					exit(EXIT_FAILURE);
				}
				int n = strlen(argv[i]);
				strncpy(save_fname, argv[i], FSIZE);
				save_fname[n] = '\0';
				break;
			case 3:
				if (argck(argv, i, apargs, sizeof(apargs) /
						sizeof(apargs[0])) == -1) {
					print_help();
					exit(EXIT_FAILURE);					
				} else {
					break;
				}
			case 4:
				if ((srt_fn_num = argck(argv, i, apsort, sizeof(apsort) /
						sizeof(apsort[0]))) == -1) {
					print_help();
					exit(EXIT_FAILURE);					
				} else {
					break;
				}
			default:
				puts("Unknown error.");
				exit(EXIT_FAILURE);		
		}
	}

	struct list *list;
	list_init(&list);

	/* LOAD STRINGS FROM FILE */
	char *ps;
	long long i = 0;
	while ((ps = str_load(f)) != NULL) {
		node_push(list, ps);
		++i;
	}
	fclose(f);
	
	/* ALLOCATE MEMORY FOR ARRAY OF POINTERS */
	char **ap;
	long long num_of_strgs = i;
	if ((ap = malloc(num_of_strgs * sizeof(ps))) == NULL) {
		puts("Not enough memory.");
		exit(EXIT_FAILURE);
	}

	/* ASSIGN STRINGS FROM LIST TO *AP[] */
	i = 0;
	while (!list_empty(list)) {
		node_pull(&ap[i], list);
		++i;
	}
	list_free(&list);

	/* SORT */
	sort(ap, num_of_strgs, srt_fn_num);
	
	/* SAVE FILE */
	if ((f = fopen(save_fname, "w")) == NULL) {
		puts("Unable to save the file.");
		exit(EXIT_FAILURE);
	}
	str_save(f, ap, num_of_strgs);
        fclose(f);

	/* FREE MEMORY */
	for (i = 0; i < num_of_strgs; ++i)
		free(ap[i]);
	free(ap);

	return 0;
}

int argck(char **argv, int i, char **ap, int len)
{
	for (int j = 0; j < len; ++j)
		if (!strcmp(argv[i], ap[j]))
			return j;
	return -1;
}

void print_help(void)
{
	printf("\nUsage:\n"
		"sort input_file output_file --sort [option]\n\n"
		"options:\n"
		"aa - ASCII ascending\n"
		"ad - ASCII descending\n"
		"wa - word length ascending\n"
		"wd - word length descending\n"
		"sa - string length ascending\n"
		"sd - string length descending\n\n"
		"sort -h print this message.\n\n");
}

void str_save(FILE *f, char **ap, const long long len)
{
	for (long long i = 0; i < len; ++i) {
		fputs(ap[i], f);
	}
}
